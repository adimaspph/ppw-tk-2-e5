$(document).ready(function(e){
    var id = $('#address').text();
    getJawab(id);
})

$("#simpan").click(function(){
    var name = $('#id_nama').val();
    var answer = $('#id_jawaban').val();
    var id = $('#address').text();
    postJawab(name, answer, id);
})

$(".my-searchbox").keyup(function (e) { 
    var id = $('#address').text();
    searchBar($("#search").val(), id);
});

function searchBar(str, id) {
    $.ajax({
        url: "/jawab/getjawab",
        contentType: "application/json",
        dataType: 'json',
        success: function(response) {
            var json = response.data;
            var searchbox = $(".searching");
            var inner = "";
            var result="";
            var nama = "";
            var jawaban = "";

            for(let i=0; i < json.length; i++){
                var nama = json[i].nama;
                var jawaban = json[i].jawaban;
                var address = json[i].tanya_id;
                console.log("address is " + address);
                if (id == address && nama.includes(str) && str !== "") {    
                    result = '<div class="containing"\>\
                    <h2 style="color: #0d1d66;">' + nama +  '<span style="font-weight: normal" > menjawab:</span></h2\>\
                    <p\>' + jawaban + '</p\> </div><br>'
    
                    inner += result;
                }
                searchbox.html(inner);
            }
        }
    })
}

function getJawab(id) {
    $.ajax({
        url: "/jawab/getjawab",
        contentType: "application/json",
        dataType: 'json',
        success: function(response) {
            var json = response.data;
            var card = $('#card');
            var inner = "";
            var result="";
            var nama = "";
            var jawaban = "";

            for(let i=0; i < json.length; i++){
                var nama = json[i].nama;
                var jawaban = json[i].jawaban;
                var address = json[i].tanya_id;
                if (id == address) {    
                    result = '<div class="containing"\>\
                    <h2 style="color: #0d1d66;">' + nama +  '<span style="font-weight: normal" > menjawab:</span></h2\>\
                    <p\>' + jawaban + '</p\> </div>'
    
                    inner += result;
                }
            }
            card.html(inner);
        }
    })
}
function postJawab(name, answer, id) {
    $.ajax({
        url: "/jawab/tambahJawaban/simpan/" + id + "/" + name + "/" + answer,
        contentType: "application/json",
        dataType: 'json',
        success: function(response) {
            var json = response.data;
            var card = $('#card');
            var inner = "";
            var result="";
            var nama = "";
            var jawaban = "";

            for(let i=0; i < json.length; i++){
                var nama = json[i].nama;
                var jawaban = json[i].jawaban;
                var address = json[i].tanya_id;
                if (id == address) {
                    result = '<div class="containing"\>\
                    <h2 style="color: #0d1d66;">' + nama +  '<span style="font-weight: normal" > menjawab:</span></h2\>\
                    <p\>' + jawaban + '</p\> </div>'
    
                    inner += result;
                }

            }
            card.html(inner);
        }
    })
}