from django.forms.formsets import formset_factory
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from jawab.forms import FormJawaban
from tanya.forms import formTanya
from jawab.models import Jawab
from tanya.models import Tanya

# Create your views here.
def jawabs(request):
    pertanyaan = Tanya.objects.all()
    response = {
        'pertanyaan': pertanyaan,
    }
    return render(request, 'jawab/questions.html', response)

def tambahJawaban(request, id):
    tanya = Tanya.objects.get(id=id)
    jawaban = Jawab.objects.filter(tanya=id)
    response = {
        "form_peserta" : FormJawaban,
        "id": id,
        "pertanyaan": tanya,
        'jawabans' : jawaban
    }
    return render(request, 'jawab/add.html', response)

def simpan(request, id, nama, jawaban):
    tanya = Tanya.objects.get(id=id)
    ans = Jawab(tanya=tanya, nama=nama, jawaban=jawaban)
    ans.save()
    return getjawab(request)

def getjawab(request):
    json = list(Jawab.objects.values())
    json.reverse() 
    return JsonResponse({'data' : json}, safe=False)