from django.urls import path

from . import views

app_name = 'jawab'

urlpatterns = [
    path('', views.jawabs, name='jawabs'),
    path('tambahJawaban/<int:id>', views.tambahJawaban, name='tambahJawaban'),
    #path('tambahJawaban/save/<int:id>', views.save, name='save'),  
    path('tambahJawaban/simpan/<int:id>/<str:nama>/<str:jawaban>', views.simpan, name='simpan'),
    path('getjawab/', views.getjawab, name='getjawab')
]