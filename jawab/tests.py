from django.test import TestCase, LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from django.test import TestCase
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from tanya.models import Tanya
from .models import *
from .forms import *
from .views import *
from .apps import JawabConfig
import unittest

class JawabConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(JawabConfig.name, 'jawab')
class ModelTest(TestCase):

    def setUp(self):
        tanya = Tanya.objects.create(nama='Jane', pertanyaan = 'Berapakah 2 + 2?')
        self.jawab = Jawab.objects.create(tanya = tanya, nama = 'John', jawaban = 'Jawabannya setauku 4')

    def test_instance_created(self):
        self.assertEqual(Jawab.objects.count(), 1)

    def test_str(self):
        tanya = Tanya.objects.create(nama='Jane', pertanyaan = 'Berapakah 2 + 2?')
        res = Jawab.objects.create(tanya = tanya, nama = 'John', jawaban = 'Jawabannya setauku 4')
        result = Jawab.objects.get(id=1)
        self.assertEqual(str(result), 'John')

class ViewsTest(TestCase):
    def setUp(self):
        tanya = Tanya.objects.create(nama='Jane', pertanyaan = 'Berapakah 2 + 2?')
        self.jawab = Jawab.objects.create(tanya = tanya, nama = 'John', jawaban = 'Jawabannya setauku 4')
        self.jawabs = reverse('jawab:jawabs')
        self.getjawab = reverse('jawab:getjawab')
        self.tambahJawaban = reverse('jawab:tambahJawaban', args=[self.jawab.id])

    def test_GET_jawabs(self):
        response = self.client.get(self.jawabs)
        self.assertEqual(response.status_code, 200)

    def test_GET_simpan(self):
        response = self.client.get(self.getjawab)
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Jawabannya', isi_html_kembalian)

    def test_getjawab_func(self):
        found = resolve('/jawab/getjawab/')
        self.assertEqual(found.func, getjawab)
        

    def test_GET_tambahJawaban(self):
        response = self.client.get(self.tambahJawaban)
        self.assertEqual(response.status_code, 200)

    def test_GET_LoggedIn(self):
        user = User.objects.create_user(username='XXX', password="FASILkom123")
        user.save()
        self.client.login(username='XXX', password="FASILkom123")
        response = self.client.get(self.tambahJawaban)
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Nama:', isi_html_kembalian)
    
    def test_GET_notLoggedIn(self):
        response = self.client.get(self.tambahJawaban)
        self.assertContains(response, "Maaf, Login dahulu untuk menjawab pertanyaan ini")