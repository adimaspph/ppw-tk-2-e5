from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
import unittest

from .models import Tanya
from .views import showTanya
# Create your tests here.
class TanyaTestCase(TestCase):

    def test_url_tanya(self):
        response = Client().get('/tanya/')
        self.assertEqual(response.status_code, 200)

    def test_tanya_templates(self):
        response = Client().get('/tanya/')
        self.assertTemplateUsed(response, 'tanya/forms.html')

    def test_views_tanya(self):
        asked = resolve('/tanya/')
        self.assertEqual(asked.func, showTanya)

    #  Tes Autentikasi
    def test_user_loggedin(self):
        user_login = User.objects.create_user(username="mammamia", password="PEPewe456")
        user_login.save()
        self.client.login(username="mammamia", password="PEPewe456")
        response = self.client.get('/tanya/')
        self.assertContains(response, "Tanya")

    def test_user_not_loggedin(self):
        response = self.client.get('/tanya/')
        self.assertContains(response, "Maaf, Login dahulu untuk menanyakan pertanyaanmu")

    # Tes model masuk setelah isi form
    def test_add_user(self):
        response = self.client.post('/tanya/', data={'nama':"mammamia", 'pertanyaan':"apakah saya bisa menyanyi?"})
        count_all_activity = Tanya.objects.all().count()
        self.assertEqual(count_all_activity, 1)