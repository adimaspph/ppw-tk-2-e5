from django.shortcuts import render
from main.forms import CreateUserForm
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def home(request):
    return render(request, 'main/lendingPage.html')

def loginUser(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            messages.info(request, 'Username ATAU Password Salah')

    return render(request, 'main/login.html')

def logoutUser(request):
    logout(request)
    return HttpResponseRedirect('/')