from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth import login
from register.models import AdditionalUserModel

from .views import *
from .urls import *

class LendingPageTestCase(TestCase):

    def test_views_home(self):
        found = resolve('/')            
        self.assertEqual(found.func, home)

    def test_is_lendingpage_exists(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/lendingPage.html')
    
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def test_login_view(self):
        response = self.client.post('/login/', {'username':'anaelsa', 'password' : 'tutifruti'})
        self.assertEqual(response.status_code, 200)

    def test_login_berhasil(self):
        AdditionalUserModel.objects.create_user(username='Ryan', password='Pacilkom123')
        
        user_login = {
            'username' : 'Ryan', 
            'password' : 'Pacilkom123'
        }

        response = self.client.get('/login/', user_login)
        self.assertEquals(response.status_code, 200)

    def test_logout(self):
        # Log in
        self.client.login(username='XXX', password="XXX")
        # Check response code
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
        # Log out
        self.client.logout()
        # Check response code
        response = self.client.get('/logout/')
        self.assertEquals(response.status_code, 302)

