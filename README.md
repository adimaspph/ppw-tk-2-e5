# Tugas Kelompok 2 (E05)

Repositori ini berisi berisi proyek Tugas Kelompok 2 yang 
dibuat oleh kelompok E05.

## Daftar Isi
- Nama Anggota Kelompok
- Link Herokuapp
- Cerita Aplikasi dan Manfaatnya
- Daftar Fitur

## Nama Anggota Kelompok

1. Adimas Putra Pratama Hendrata    (1906305575)
2. Affan Mirza Chairy               (1906302516)
3. Khusnul Khotimah                 (1906298986)
4. Rafi Ismail                      (1906302491)
5. Ryan Dirga Aidil Hakim           (1906306741)

## Link Herokuapp

`http://13.229.231.57:8000/`** atau [klik][link-herokuapp]

username: adimaspph
password: Login@2023

## Cerita Aplikasi dan Manfaatnya

Aplikasi yang kami buat bernama **Findterest**. Findterest terdiri dari dua kata, 
yaitu *find* dan *interest*. Jadi melalui aplikasi ini, seseorang dapat
mencari relasi berdasarkan *interest* yang sama dan juga
berbagi pengalaman tentang *interest* nya masing-masing. 

Target user dari Findterest adalah mahasiswa dan freshgraduate. Melalui Findterest, 
seseorang dapat membagi keluh kesahnya selama menjalani perkuliahannya maupun
dalam mencari pekerjaan. 

Kami juga berharap Findterest dapat menjadi wadah untuk berbagi ilmu pengetahuan, 
membantu mahasiswa yang kesulitan mengerjakan tugasnya, dan penyelesaian masalah lain 
melalui fitur yang kami sediakan. 


## Daftar fitur

1. **Find friends**
    User dapat mencari teman yang memiliki persamaan *intereset*, kampus, dan jurusan.
2. **Register**
    User dapat berkontribusi kepada Findterest dengan mendaftarkan diri. Data diri ini akan kami jadikan
    database di fitur **find friends**
3. **Curhat**
    User dapat mencurahkan perasaannya disini. Mereka dapat mencantumkan nama ataupun anonim.
4. **Tanya**
    User dapat bertanya disini. Pertanyaannya dapat seputar perkuliahan, dunia kerja, dan
    hal lain. Sama seperti curhat, mereka dapat mencantumkan nama ataupun anonim. 
5. **Jawab**
    User dapat menjawab pertanyaan yang diajukan user lain. Dalam menjawab juga dapat mencantumkan 
    nama ataupun anonim.


[link-herokuapp]: http://13.229.231.57:8000/
[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
