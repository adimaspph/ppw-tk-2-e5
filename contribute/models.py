from django.db import models

TAHUN = [
    ('2020', '2020'),
    ('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('Alumni', 'Alumni'),
]

class YearOfEntry(models.Model):
    year_of_entry = models.CharField(max_length = 10, null = True)
    def __str__(self):
        return self.year_of_entry

class ContributeModel(models.Model):
    image_url = models.URLField(max_length = 100, blank = True, null = True)
    name = models.CharField(max_length = 50, null = True)
    interest = models.CharField(max_length = 50, null = True)
    major = models.CharField(max_length = 50, null = True)
    university = models.CharField(max_length = 50, null = True)
    year_of_entry_cont= models.CharField(max_length=20, choices=TAHUN, default="2020")
    social_media = models.CharField(max_length = 50)

    def __str__(self):
        return self.name 