from django.shortcuts import render, redirect
from .forms import Findform, TahunForm
from .models import Find
from contribute.models import ContributeModel
from register.models import AdditionalUserModel
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import JsonResponse
import json
from pip._vendor import requests
from django.core import serializers


# Create your views here.
def searchfriends(request):
    form = Findform()
    formtahun = TahunForm()
    if request.method == "POST":
        form = Findform(request.POST)
        if form.is_valid():
            return redirect('/friendlist/')
    else:
        form = Findform()

    return render(request, 'search.html', {'form' : form, 'formtahun' : formtahun})

def friendlist(request):
    form = Findform(request.POST)
    formtahun = TahunForm(request.POST)
    tahun = formtahun['tahun'].value()
    # univ = form['university'].value()
    jurusan = form['major'].value()
    hasil = AdditionalUserModel.objects.filter(year_of_entry=str(tahun), major=jurusan)
    return render(request, 'list.html', {'form' : form, 'formtahun' : formtahun, 'hasil' : hasil})

def data(request):
    url_destination = request.GET['q']
    jeson = serializers.serialize('json', AdditionalUserModel.objects.filter(university__icontains=url_destination))
    data2 = json.loads(jeson)
    return JsonResponse(data2, safe=False)