from django.test import TestCase, Client
from django.urls import reverse, resolve

from django.apps import apps
from findfriends.apps import FindfriendsConfig

from register.models import AdditionalUserModel
from .models import Find
from .views import *
from django.http import request

# Create your tests here.

# TEST APP
class FindfriendsConfigTest(TestCase):
    def test_app(self):
        self.assertEqual(FindfriendsConfig.name, 'findfriends')
        
class FindTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    # SEARCHFRIENDS   
    # Test url
    def test_url_searchfriends(self):
       response = Client().get('/findfriends/searchfriends/')
       self.assertEqual(response.status_code, 200)
    
    # Test template used
    def test_searchfriends_template(self):
        response = Client().get('/findfriends/searchfriends/')
        self.assertTemplateUsed(response, 'search.html')
    
    def test_login_view(self):
        response = self.client.post('/login/', {'username':'Ryan', 'password' : 'Pacilkom123'})
        self.assertEqual(response.status_code, 200)
    
    def test_view_dalam_template_searchfriends(self):
        user = AdditionalUserModel.objects.create_user(username='Ryan', password='Pacilkom123')
        user.save()
        self.client.login(username='Ryan', password='Pacilkom123')

        response = self.client.get('/findfriends/searchfriends/')
        isi_view = response.content.decode('utf8')
        self.assertIn("Carilah temanmu berdasarkan kategori berikut :", isi_view)   
        self.assertIn('<button type="submit" value="Save" class="button"><h4>SEARCH</h4></button>', isi_view)


    # Test views
    def test_views_searchfriends(self):
        found = resolve('/findfriends/searchfriends/')            
        self.assertEqual(found.func, searchfriends)


    # FRIENDLIST    
    # Test url
    def test_url_friendlist(self):
        response = Client().get('/findfriends/friendlist/')
        self.assertEqual(response.status_code, 200)
    
    def test_friendlist_post_url(self):
        response = Client().post('/findfriends/friendlist/', data={'first_name': 'Ryan Dirga', "interest" : "main bola"})
        amount = AdditionalUserModel.objects.filter(first_name = "Ryan Dirga").count()
        self.assertEqual(amount, 0)
    
    # Test template used
    def test_friendlist_template(self):
        response = Client().get('/findfriends/friendlist/')
        self.assertTemplateUsed(response, 'list.html')
    
    # Test views
    def test_views_friendlist(self):
        found = resolve('/findfriends/friendlist/')            
        self.assertEqual(found.func, friendlist)
    
    def test_view_dalam_template_friendlist(self):
        response = Client().get('/findfriends/friendlist/') 
        isi_view = response.content.decode('utf8')
        self.assertIn("Hasil Pencarian", isi_view)
        self.assertIn("Semoga ada teman yang sesuai :)", isi_view)

    def test_search_friendlist(self):
        response = Client().get('/findfriends/friendlist/data/?q=')
        hasil = AdditionalUserModel.objects.all()
        self.assertEqual(response.status_code, 200)
        
    # Test model      
    def test_check_model_searchfriend(self):
        AdditionalUserModel.objects.create(university="UI", major="Sistem Informasi")
        amount = AdditionalUserModel.objects.all().count()
        self.assertEqual(amount, 1)

    def test_check_model_searchfriend_tahun(self):
        Find.objects.create(tahun=2019)
        amount = Find.objects.all().count()
        self.assertEqual(amount, 1)