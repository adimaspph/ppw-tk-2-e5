from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Curhat
from .forms import FormCurhat
from register.models import AdditionalUserModel
from django.http import JsonResponse


# Create your views here.
def curhat(request):
    
    isi_curhat = Curhat.objects.all()
    response = {
        "curhats": isi_curhat,
        "form_curhat": FormCurhat,
    }
    
    # form = FormCurhat(request.POST or None)
    
    # if (form.is_valid() and request.method == 'POST' and request.user.is_authenticated):
    #     form.save()

    return render(request, 'curhat/home.html', response)


def get_curhat(request):
    json = list(Curhat.objects.values())
    json.reverse()
        
    return JsonResponse({'data' : json}, safe=False)
    

def post_curhat(request, nama, isinya):

    if (request.user.is_authenticated):
        form = Curhat(dari = nama, isi = isinya)   
        form.save()

    return get_curhat(request)