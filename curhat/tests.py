from django.test import TestCase, Client
from django.urls import reverse, resolve
from register.models import AdditionalUserModel
from django.contrib.auth import authenticate, login, logout

# from .models import Curhat
from .views import *
from .urls import *

class CurhatTestCase(TestCase):
    
    # Views test
    def test_views_curhat(self):
        found = resolve('/curhat/')        
        self.assertEqual(found.func, curhat)

    def test_is_curhat_page_exists(self):
        response = Client().get('/curhat/')
        self.assertTemplateUsed(response, 'curhat/home.html')

    # Url test
    def test_curhat_create_url_exists(self):
        response = Client().get('/curhat/')
        self.assertEqual(response.status_code, 200)

    # def test_tambah_curhat_post_url(self):
    #     # Create User
    #     user1 = AdditionalUserModel.objects.create_user(username="username1", first_name= "test", password='testUsernamePassword')

    #     user = authenticate(username = "username1", password = 'testUsernamePassword')
    #     response = self.client.post('/login/', {'username':'username1', 'password' : 'testUsernamePassword'})

    #     response = self.client.post('/curhat/', data={"isi" : "gk tau isinya"})
    #     amount = Curhat.objects.filter(dari = "test").count()
        
    #     self.assertEqual(amount, 1)
    #     self.assertEqual(response.status_code, 200)

    def test_tambah_curhat(self):
        # Create User
        user1 = AdditionalUserModel.objects.create_user(username="username1", first_name= "test", password='testUsernamePassword')

        user = authenticate(username = "username1", password = 'testUsernamePassword')
        response = self.client.post('/login/', {'username':'username1', 'password' : 'testUsernamePassword'})
        result = self.client.get('/curhat/post-curhat/adimas/hahaha/')
        
        amount = Curhat.objects.all().count()
        self.assertEqual(amount, 1)
        self.assertEqual(result.status_code, 200)

    def test_get_json_curhat(self):
        Curhat.objects.create(dari="Anton", isi="lagi sedih")
        response = self.client.get('/curhat/get-curhat/')
        content = response.content.decode('utf-8')
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'data': [{'dari': 'Anton', 'id': 1, 'isi': 'lagi sedih'}]}
        )
        self.assertEqual(response.status_code, 200)

    # Module test
    def test_check_model_create_curhat(self):
        Curhat.objects.create(dari="Anton", isi="lagi sedih")
        amount = Curhat.objects.all().count()
        self.assertEqual(amount, 1)

    def test_check_model_return_curhat(self):
        Curhat.objects.create(dari="Anton",  isi="lagi sedih")
        result = Curhat.objects.get(id=1)
        self.assertEqual(str(result), "Anton")
