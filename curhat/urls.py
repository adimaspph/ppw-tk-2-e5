from django.urls import include, path
from . import views

app_name = 'curhat'

urlpatterns = [
    path('', views.curhat, name='home'),
    path('get-curhat/', views.get_curhat, name='get-curhat'),
    # path('post/', views.post_curhat, name='post'),
    path('post-curhat/<str:nama>/<str:isinya>/', views.post_curhat),
]
