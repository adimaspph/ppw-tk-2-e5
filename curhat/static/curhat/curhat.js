$(document).ready(function(){
    getCurhat()
})

$("#curhatkan").click(function(){
    var dari = $('#id_dari').val();
    var isi = $('#id_isi').val();
    // console.log(dari)
    $('#id_dari').val('');
    $('#id_isi').val('');
    
    if (dari != "" && isi != "") {
        postCurhat(dari, isi)
    } else if (dari == "" && isi != "") {
        postCurhat("Anonymous", isi)
    }
    
    
    // console.log(dari)
})

// #0092ff

$("#id_isi").keyup(function(){
    var isi = $('#id_isi').val();
    var button = document.getElementById("curhatkan");
    if (isi != "") {
        button.style.backgroundColor = "#0092ff"
    } else {
        button.style.backgroundColor = "#666666"
    }
    
})

$("#curhatkan").click(function(){
    window.scrollBy(0, 500);
    var button = document.getElementById("curhatkan");
    button.style.backgroundColor = "#666666"
    
})

$(document).ready(function(){
    window.scrollBy(0, 100);
    var button = document.getElementById("curhatkan");
    button.style.backgroundColor = "#666666"
    // var elmnt = document.getElementById("resultBox");
    // elmnt.scrollIntoView(true);
})


function getCurhat() {
    $.ajax({
        url: "/curhat/get-curhat",
        contentType: "application/json",
        dataType: 'json',
        success: function(response){
            let resultBox = $("#resultBox")
            let inner = ""
            let json = response.data
            let isi = "";
            let dari = "";
            let result = "";
            for(let i=0; i < json.length; i++){
                isi = json[i].isi;
                dari = json[i].dari;
                

                result = '<div class="card"\>\
                <h2 class="judul">Dari <span class="biru"> ' + dari + ' </span></h2\>\
                <p\>' + isi + '</p\> </div>'

                inner += result
            }
            resultBox.html(inner)
        }
    })
}

function postCurhat(nama, isinya) {
    $.ajax({
        url: "/curhat/post-curhat/" + nama + "/" + isinya,
        contentType: "application/json",
        dataType: 'json',
        success: function(response){
            let resultBox = $("#resultBox")
            let inner = ""
            let json = response.data
            let isi = "";
            let dari = "";
            let result = "";
            for(let i=0; i < json.length; i++){
                isi = json[i].isi;
                dari = json[i].dari;
                

                result = '<div class="card"\>\
                <h2 class="judul">Dari <span class="biru"> ' + dari + ' </span></h2\>\
                <p\>' + isi + '</p\> </div>'

                inner += result
            }
            resultBox.html(inner)
        }
    })
}
