from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth import login, get_user_model
from django.contrib.auth.models import User

from .views import *
from .urls import *

class TestRegister(TestCase):
    def test_register_url(self):
        response = Client().get('/register/')
        self.assertEquals(200, response.status_code)
    
    def test_POST_form(self):
        data = {
            'username':'amandamanopo1',
            'first_name':'Amanda',
            'password1':'ASDFGHjklABC123',
            'password2':'ASDFGHjklABC123',
            # 'image_url' : 'https://cdn.idntimes.com/content-images/post/20201019/amandamanopo-20201019-093340-0-bc0e840f3d8e63b0db09cfbeacd04c1b_600x400.jpg', 
            # 'interest' : 'Sleep', 
            # 'major': 'Sistem informasi', 
            # 'university': 'UI', 
            # 'year_of_entry': '2020', 
            # 'social_media' : '@siapaya',
        }

        response = Client().post('/register/', data)
        
    # Test Halaman Profile
    def test_view_dalam_template_profile(self):
        user = AdditionalUserModel.objects.create_user(username='Ryan', password='Pacilkom123')
        user.save()
        self.client.login(username='Ryan', password='Pacilkom123')

        response = self.client.get('/profile/')
        isi_view = response.content.decode('utf8')
        self.assertIn("Ryan", isi_view)   

 
    # def test_success_register_user(self):
    #     user_count = AdditionalUserModel.objects.all().count()
    #     user_credentials = {
    #         'username':'username',
    #         'first_name':'firstanem',
    #         'password1':'testUsernamePassword123',
    #         'password2':'testUsernamePassword123',
    #     }
    #     response = Client().post('/register', data = user_credentials, follow = True)
    #     content = response.content.decode('utf8')
    #     self.assertEqual(AdditionalUserModel.objects.all().count(), 1)
    #     self.assertIn("username", content)
    #     self.assertEqual(response.status_code, 200)
    