# Generated by Django 3.1.2 on 2021-01-01 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExtraProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ada_apa_hari_ini', models.TextField()),
            ],
        ),
    ]
