# Generated by Django 3.1.3 on 2020-12-22 05:09

import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalUserModel',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='auth.user')),
                ('image_url', models.URLField(blank=True, max_length=100, null=True)),
                ('interest', models.CharField(max_length=50, null=True)),
                ('major', models.CharField(max_length=50, null=True)),
                ('university', models.CharField(max_length=50, null=True)),
                ('year_of_entry', models.CharField(choices=[('2020', '2020'), ('2019', '2019'), ('2018', '2018'), ('2017', '2017'), ('2016', '2016'), ('2015', '2015'), ('Alumni', 'Alumni')], default='2020', max_length=20)),
                ('social_media', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            bases=('auth.user',),
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
