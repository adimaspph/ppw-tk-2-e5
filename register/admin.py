from django.contrib import admin
from .models import AdditionalUserModel, ExtraProfile

admin.site.register(AdditionalUserModel)
admin.site.register(ExtraProfile)
