from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from register.models import AdditionalUserModel, ExtraProfile
from django import forms

class CreateUserForm(UserCreationForm):
    class Meta:
        model = AdditionalUserModel
        fields = ['username', 'first_name', 'password1', 'password2', 'image_url', 'interest', 'major', 'university', 'year_of_entry', 'social_media']

        widgets = {
            'year_of_entry' : forms.Select(attrs = {'class' : 'form-control',
                                            'type' : 'option'})}
    
    username = forms.CharField(
        widget=forms.TextInput(attrs = {
        'placeholder': 'Username',
        'type' : 'text',
        }))

    first_name = forms.CharField(
        widget=forms.TextInput(attrs = {
        'placeholder': 'Surname',
        'type' : 'text',
        }))
    
    password1 = forms.CharField(
        widget=forms.TextInput(attrs = {
        'placeholder': 'Password',
        'type' : 'password',
        }))
    
    password2 = forms.CharField(
        widget=forms.TextInput(attrs = {
        'placeholder': 'Re-enter Password',
        'type' : 'password',
        }))
    
    image_url = forms.URLField(
        required = False, widget=forms.URLInput(attrs = {
        'type' : 'url', 'placeholder' : 'Profile Picture URL',
        }))

    interest = forms.CharField(
        required = False,
        widget=forms.TextInput(attrs = {
        'placeholder': 'Interest',
        'type' : 'text',
        }))

    university = forms.CharField(
        required = False,
        widget=forms.TextInput(attrs = {
        'placeholder': 'Last/Current University',
        'type' : 'text',
        }))

    major = forms.CharField(
        required = False,
        widget=forms.TextInput(attrs = {
        'placeholder': 'Last/Current Major',
        'type' : 'text',
        }))
    
    social_media = forms.CharField(
        required = False,
        widget=forms.TextInput(attrs = {
        'placeholder': 'Social Media. e.g. @kudaponi (ig)',
        'type' : 'text',
        }))

class ExtraProfileForm(forms.ModelForm):
    class Meta:
        model = ExtraProfile
        fields = "__all__"

    ada_apa_hari_ini = forms.CharField(
        widget=forms.Textarea(attrs = {
        'placeholder': 'Tulis apapun yang ada di pikiranmu saat ini',
        'type' : 'text',
        }))

