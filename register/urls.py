from django.urls import path

from . import views

app_name = 'register'

urlpatterns = [
    path('register/', views.registerUser, name='register'),
    path('profile/', views.profileUser, name='profile'),
]