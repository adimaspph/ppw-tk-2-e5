from django.db import models
from django.contrib.auth.models import User

TAHUN = [
    ('2020', '2020'),
    ('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('Alumni', 'Alumni'),
]

class AdditionalUserModel(User):
    image_url = models.URLField(max_length = 100, blank = True, null = True)
    interest = models.CharField(max_length = 50, null = True)
    major = models.CharField(max_length = 50, null = True)
    university = models.CharField(max_length = 50, null = True)
    year_of_entry = models.CharField(max_length=20, choices=TAHUN, default="2020")
    social_media = models.CharField(max_length = 50)

class ExtraProfile(models.Model):
    ada_apa_hari_ini = models.TextField()