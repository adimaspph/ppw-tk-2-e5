from django.shortcuts import render
from register.forms import CreateUserForm, ExtraProfileForm
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from register.models import AdditionalUserModel, ExtraProfile

def registerUser(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        print(form)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(request, username = username, password = password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
   
        else:
            messages.info(request, 'Username atau password tidak valid')

    response = {'form':form}
    return render(request, 'register/register.html', response)

def profileUser(request):
    form = ExtraProfileForm(request.POST or None)
    if(form.is_valid() and request.method == 'POST'):
        form.save()
        print("save")
    response = {'form':form}
    return render(request, 'register/profile.html', response)
